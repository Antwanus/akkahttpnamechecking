package com.vpp;

import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.server.Route;
import akka.http.javadsl.testkit.TestRoute;
import akka.http.javadsl.testkit.TestRouteResult;
import akka.http.javadsl.testkit.JUnitRouteTest;
import org.junit.Test;

import static org.junit.Assert.*;

public class ApplicationToTestTest extends JUnitRouteTest {

   @Test
   public void testAddingIsOk() {
      Route route = new ApplicationToTest().getRoutes();
      TestRoute testRoute = testRoute(route);
      HttpRequest request = HttpRequest.GET("/add?first=4&second=19");
      TestRouteResult testRouteResult = testRoute.run(request);
      testRouteResult.assertStatusCode(200);
      testRouteResult.assertEntity("{\"total\" : 23}");
      // ....
   }

}